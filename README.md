# Mastermind Helper

## What is this helping tool for?

This little tool helps to get an overwiew of all posible combinations in a particular version of Mastermind, a popular game invented somewhere in the 70's.

## Usage

Run it with ``./mastermind`` . It will initialize immediately.
Enter a combination of 4 digits a, b, c, d and the values p and f, all separated by spaces. a, b, c, d can take the value 0..9 whereas p and f can stand for 0..4 .
This all makes sense if you play against a partner who wrote down their own combination of four digits, all of them being different from each other. They have to compare their combo to the one you told them. 
Then they'll answer to you how many digits are an element of their combo but are standing in another place than in your combo (that is p) and how many digits are part of their combo and are standing in the same place as in your combo (thats f).
You have to guess until you find the right digits or until you give up. This helping hand will support you. Good luck!