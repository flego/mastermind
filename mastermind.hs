import Control.Monad
import Data.List

main = do
    putStrLn "Mastermind Helper v0.1"
    putStrLn "Allowed digits are 0-9 for a, b, c, d."
    putStrLn "New game started..."
    game (listGen)
    return ()

game (seed) = do
    line <- prompt "Type a combination <a b c d p f> or <0> to give up: "
    let comb = map (read :: String -> Int) line
    when (comb /= [(0)] && (length (comb) == 6)) $ do
        let listCurr = listComb seed comb
        print listCurr
        when (listCurr /= []) $ do
            game (listCurr)
    return ()

prompt :: String -> IO ([String])    
prompt s = do
        putStrLn s
        line <- getLine
        return (words line)

listGen :: [[Int]]
listGen = [[u,v,w,x] | u <- [0..9], v <- [0..9], w <- [0..9], x <- [0..9], u /= v, u /= w, u /= x, v /= w, v /= x, w /= x]

listComb :: [[Int]] -> [Int] -> [[Int]]
listComb listCurr [a, b, c, d, p, f]
    | any (<0) [a,b,c,d,p,f] == True = error "Negative digits are not allowed."
    | any (>9) [a,b,c,d,p,f] == True = error "Only digits are allowed."
    | a == b                         = error "Identical digits detected. a, b, c, d must be different."
    | a == c                         = error "Identical digits detected. a, b, c, d must be different."
    | a == d                         = error "Identical digits detected. a, b, c, d must be different."
    | b == c                         = error "Identical digits detected. a, b, c, d must be different."
    | b == d                         = error "Identical digits detected. a, b, c, d must be different."
    | c == d                         = error "Identical digits detected. a, b, c, d must be different."
    
    | p == 0 && f == 4               = [[a,b,c,d]]
    | p == 0 && f == 3               = [[u,v,w,x] | [u,v,w,x] <- listCurr, 
                                                    (u == a && v == b && w == c) || 
                                                    (u == a && v == b && x == d) || 
                                                    (u == a && w == c && x == d) || 
                                                    (v == b && w == c && x == d), 
                                                    [u,v,w,x] /= [a,b,c,d]]
                                                    
    | p == 0 && f == 2               = [[u,v,w,x] | [u,v,w,x] <- listCurr, 
                                                    (u == a && v == b) || 
                                                    (u == a && w == c) || 
                                                    (u == a && x == d) || 
                                                    (v == b && w == c) || 
                                                    (v == b && x == d) || 
                                                    (w == c && x == d), 
                                                    if (u == a && v == b) then (w /= a && w /= b && w /= c && w /= d && x /= a && x /= b && x /= c && x /= d) else
                                                    if (u == a && w == c) then (v /= a && v /= b && v /= c && v /= d && x /= a && x /= b && x /= c && x /= d) else
                                                    if (u == a && x == d) then (v /= a && v /= b && v /= c && v /= d && w /= a && w /= b && w /= c && w /= d) else
                                                    if (v == b && w == c) then (u /= a && u /= b && u /= c && u /= d && x /= a && x /= b && x /= c && x /= d) else
                                                    if (v == b && x == d) then (u /= a && u /= b && u /= c && u /= d && w /= a && w /= b && w /= c && w /= d) else (u /= a && u /= b && u /= c && u /= d && v /= a && v /= b && v /= c)]
                                                    
    | p == 0 && f == 1               = [[u,v,w,x] | [u,v,w,x] <- listCurr, 
                                                    (u == a) || (v == b) || (w == c) || (x == d), 
                                                    if u == a then 
                                                        v /= a && v /= b && v /= c && v /= d && w /= a && w /= b && w /= c && w /= d && x /= a && x /= b && x /= c && x /= d else
                                                    if u == b then 
                                                        u /= a && u /= b && u /= c && u /= d && w /= a && w /= b && w /= c && w /= d && x /= a && x /= b && x /= c && x /= d else
                                                    if u == c then 
                                                        u /= a && u /= b && u /= c && u /= d && v /= a && v /= b && v /= c && v /= d && x /= a && x /= b && x /= c && x /= d else u /= a, u /= b, u /= c, u /= d, v /= a, v /= b, v /= c, v /= d, w /= a, w /= b, w /= c, w /= d]
                                                        
    | p == 0 && f == 0               = [[u,v,w,x] | [u,v,w,x] <- listCurr, 
                                                    u /= a, u /= b, u /= c, u /= d, v /= a, v /= b, v /= c, v /= d, w /= a, w /= b, w /= c, w /= d, x /= a, x /= b, x /= c, x /= d]

    | p == 1 && f == 2               = [[u,v,w,x] | [u,v,w,x] <- listCurr, 
                                                    u == a && v == b && w == d && x /= c || -- f f p _
                                                    u == a && v == b && w /= d && x == c || -- f f _ p
                                                    u == a && v == d && w == c && x /= b || -- f p f _
                                                    u == a && v /= d && w == c && x == b || -- f _ f p
                                                    u == a && v == c && w /= b && x == d || -- f p _ f
                                                    u == a && v /= c && w == b && x == d || -- f _ p f
                                                    u == d && v == b && w == c && x /= a || -- p f f _
                                                    u /= d && v == b && w == c && x == a || -- _ f f p
                                                    u == c && v == b && w /= a && x == d || -- p f _ f
                                                    u /= c && v == b && w == a && x == d || -- _ f p f
                                                    u == b && v /= a && w == c && x == d || -- p _ f f
                                                    u /= b && v == a && w == c && x == d ]  -- _ p f f 
    
    | p == 1 && f == 1               = [[u,v,w,x] | [u,v,w,x] <- listCurr, 
                                                    u == a && v `elem` [c, d] && w `notElem` [a, b] && x `notElem` [a, b] || -- f p _ _
                                                    u == a && v `notElem` [a, c] && w `elem` [b, d] && x `notElem` [a, c] || -- f _ p _
                                                    u == a && v `notElem` [a, d] && w `notElem` [a, d] && x `elem` [b, c] || -- f _ _ p
                                                    u `elem` [c, d] && v == b && w `notElem` [a, b] && x `notElem` [a, b] || -- p f _ _
                                                    u `notElem` [c, d] && v == b && w `elem` [a, d] && x `notElem` [c, d] || -- _ f P _
                                                    u `notElem` [b, d] && v == b && w `notElem` [b, d] && x `elem` [a, c] || -- _ f _ p
                                                    u `elem` [b, d] && v `notElem` [a, c] && w == c && x `notElem` [a, c] || -- p _ f _
                                                    u `notElem` [b, c] && v `elem` [a, d] && w == c && x `notElem` [b, c] || -- _ p f _
                                                    u `notElem` [c, d] && v `notElem` [c, d] && w == c && x `elem` [a, b] || -- _ _ f p
                                                    u `elem` [b, c] && v `notElem` [a, d] && w `notElem` [a, d] && x == d || -- p _ _ f
                                                    u `notElem` [b, d] && v `elem` [a, c] && w `notElem` [b, d] && x == d || -- _ p _ f
                                                    u `notElem` [c, d] && v `notElem` [c, d] && w `elem` [a, b] && x == d ]  -- _ _ p f 
                                                    
    | p == 1 && f == 0               = [[u,v,w,x] | [u,v,w,x] <- listCurr, 
                                                    (a `elem` [v,w,x] && b `notElem` [u,v,w,x] && c `notElem` [u,v,w,x] && d `notElem` [u,v,w,x]) || 
                                                    (a `notElem` [u,v,w,x] && b `elem` [u,w,x] && c `notElem` [u,v,w,x] && d `notElem` [u,v,w,x]) ||
                                                    (a `notElem` [u,v,w,x] && b `notElem` [u,v,w,x] && c `elem` [u,v,x] && d `notElem` [u,v,w,x]) ||
                                                    (a `notElem` [u,v,w,x] && b `notElem` [u,v,w,x] && c `notElem` [u,v,w,x] && d `elem` [u,v,w]) ]
    
    | p == 2 && f == 2               = [[u,v,w,x] | [u,v,w,x] <- listCurr, 
                                                    u == a && v == b && w == d && x == c || -- f f p p
                                                    u == a && v == d && w == c && x == b || -- f p f p
                                                    u == a && v == c && w == b && x == d || -- f p p f
                                                    u == d && v == b && w == c && x == a || -- p f f p
                                                    u == c && v == b && w == a && x == d || -- p f p f
                                                    u == b && v == a && w == c && x == d ]  -- p p f f

    | p == 2 && f == 1               = [[u,v,w,x] | [u,v,w,x] <- listCurr, 
                                                    (u `elem` [b,c,d] && v `elem` [a,c,d] && w `notElem` [a,b,c,d] && x == d) || -- p p _ f
                                                    (u `elem` [b,c,d] && v `elem` [a,c,d] && w == d && x `notElem` [a,b,c,d]) || -- p p f _
                                                    (u `elem` [b,c,d] && v `notElem` [a,b,c,d] && w `elem` [a,b,d] && x == d) || -- p _ p f
                                                    (u `elem` [b,c,d] && v == b && w `elem` [a,b,d] && x `notElem` [a,b,c,d]) || -- p f p _
                                                    (u `elem` [b,c,d] && v `notElem` [a,b,c,d] && w == c && x `elem` [a,b,c]) || -- p _ f p
                                                    (u `elem` [b,c,d] && v == b && w `notElem` [a,b,c,d] && x `elem` [a,b,c]) || -- p f _ p
                                                    (u `notElem` [a,b,c,d] && v `elem` [a,c,d] && w `elem` [a,b,d] && x == d) || -- _ p p f
                                                    (u == a && v `elem` [a,c,d] && w `elem` [a,b,d] && x `notElem` [a,b,c,d]) || -- f p p _
                                                    (u `notElem` [a,b,c,d] && v `elem` [a,c,d] && w == c && x `elem` [a,b,c]) || -- _ p f p
                                                    (u == a && v `elem` [a,c,d] && w `notElem` [a,b,c,d] && x `elem` [a,b,c]) || -- f p _ p
                                                    (u `notElem` [a,b,c,d] && v == b && w `elem` [a,b,d] && x `elem` [a,b,c]) || -- _ f p p
                                                    (u == a && v `notElem` [a,b,c,d] && w `elem` [a,b,d] && x `elem` [a,b,c]) ]  -- f _ p p 
                                                    
    | p == 2 && f == 0               = [[u,v,w,x] | [u,v,w,x] <- listCurr, 
                                                    (u `elem` [a,b,c,d] && v `elem` [a,b,c,d] && w `notElem` [a,b,c,d] && x `notElem` [a,b,c,d]) || -- p p _ _
                                                    (u `elem` [a,b,c,d] && v `notElem` [a,b,c,d] && w `elem` [a,b,c,d] && x `notElem` [a,b,c,d]) || -- p _ p _
                                                    (u `elem` [a,b,c,d] && v `notElem` [a,b,c,d] && w `notElem` [a,b,c,d] && x `elem` [a,b,c,d]) || -- p _ _ p
                                                    (u `notElem` [a,b,c,d] && v `elem` [a,b,c,d] && w `elem` [a,b,c,d] && x `notElem` [a,b,c,d]) || -- _ p p _
                                                    (u `notElem` [a,b,c,d] && v `elem` [a,b,c,d] && w `notElem` [a,b,c,d] && x `elem` [a,b,c,d]) || -- _ p _ p
                                                    (u `notElem` [a,b,c,d] && v `notElem` [a,b,c,d] && w `elem` [a,b,c,d] && x `elem` [a,b,c,d]) ]  -- _ _ p p
    
    | p == 3 && f == 1               = [[u,v,w,x] | [u,v,w,x] <- listCurr, 
                                                    (u `elem` [b,c] && v `elem` [a,c] && w `elem` [a,b] && x == d) || -- p p p f
                                                    (u `elem` [b,d] && v `elem` [a,d] && w == d && x `elem` [a,b]) || -- p p f p
                                                    (u `elem` [c,d] && v == b && w `elem` [a,d] && x `elem` [a,c]) || -- p f p p
                                                    (u == a && v `elem` [c,d] && w `elem` [b,d] && x `elem` [b,c]) ]  -- f p p p
                                                    
    | p == 3 && f == 0               = [[u,v,w,x] | [u,v,w,x] <- listCurr, 
                                                    (u `elem` [b,c,d] && v `elem` [a,c,d] && w `elem` [a,b,d] && x `notElem` [a,b,c,d]) || -- p p p _
                                                    (u `elem` [b,c,d] && v `elem` [a,c,d] && w `notElem` [a,b,c,d] && x `elem` [a,b,c]) || -- p p _ p
                                                    (u `elem` [b,c,d] && v `notElem` [a,b,c,d] && w `elem` [a,b,d] && x `elem` [a,b,c]) || -- p _ p p
                                                    (u `notElem` [a,b,c,d] && v `elem` [a,c,d] && w `elem` [a,b,d] && x `elem` [a,b,c]) ]  -- _ p p p
                                                    
    | p == 4 && f == 0               = [[u,v,w,x] | [u,v,w,x] <- listCurr, 
                                                    (a `elem` [u,v,w,x] && b `elem` [u,v,w,x] && c `elem` [u,v,w,x] && d `elem` [u,v,w,x]),
                                                    u /= a, v /= b, w/= c, x/= d]
    
    | otherwise                      = error "That is not a possible combination, check your p/f count!"
    
